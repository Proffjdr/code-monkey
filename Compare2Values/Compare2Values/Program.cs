﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compare2Values
{
    class Program
    {
        static void Main(string[] args)
        {
            int a;
            int b;

            Console.WriteLine("Input Value 1");
            a = int.Parse(Console.ReadLine());

            Console.WriteLine("Input Value 2");
            b = int.Parse(Console.ReadLine());

            if(a < b)
            {
                Console.WriteLine($"{a} is smaller than {b}");
            }
            else if (a == b)
            {
                Console.WriteLine($"{a} is equal to {b}");
           }
            else
            {
                Console.WriteLine($"{a} is larger than {b}");
            } 

        }
    }
}
