﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace simplewhileloop
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 0;
            int count = 10;

            while ( i < count)
            {
                int a = i + 1;
                Console.WriteLine($"This is line {a}");
                i++;
            }



        }
    }
}
