﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loop
{
    class Program
    {
        static void Main(string[] args)
        {
            int i;
            int count = 10;
            for (i = 0; i < count; i++){
                int a = i + 1;
                Console.WriteLine($"This is line {a}");
            }
        }
    }
}
