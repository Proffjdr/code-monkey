﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace userinputwhile
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 0;
            int count = 20;

            Console.WriteLine("How many lines would you like?");
            count = int.Parse(Console.ReadLine());
            while (i < count)
            {
                int a = i + 1;
                Console.WriteLine($"This is line {a}");
                i++;

            }
            
        }
    }
}
