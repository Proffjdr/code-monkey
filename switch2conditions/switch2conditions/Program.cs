﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace switch2conditions
{
    class Program
    {
        static void Main(string[] args)
        {
            

                Console.WriteLine("Choose red or blue");
            var color = Console.ReadLine();

            switch (color)
            {
                case "red":
                    Console.WriteLine("You chose red - like the blood of our enemies");
                    break;

                case "blue":
                    Console.WriteLine("You chose blue - like the sky!");
                    break;

                default:
                    Console.WriteLine("You chose poorly!");
                    break;
            }
        }
    }
}
